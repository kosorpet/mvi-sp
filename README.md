# Child mind instutide - Detecting sleep states
- More detail at the competition link [here](https://www.kaggle.com/c/child-mind-institute-detect-sleep-states) or in report.pdf.

### How to replicate
- All of the training and feature engineering is present in the jupyter notebook. However this being a kaggle competition, the evalueation relies on the Average Precision metric defined by the competition. If you want to recreate the training, open the notebook in kaggle.
